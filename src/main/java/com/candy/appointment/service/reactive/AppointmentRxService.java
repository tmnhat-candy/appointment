package com.candy.appointment.service.reactive;

import com.candy.appointment.dto.AppointmentDTO;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service
public class AppointmentRxService {

    public Flux<AppointmentDTO> queryRxAppointment() {
        AppointmentDTO appointment1 = AppointmentDTO.builder()
                .id(1)
                .name("appointment-reactive-1")
                .build();

        AppointmentDTO appointment2 = AppointmentDTO.builder()
                .id(2)
                .name("appointment-reactive-2")
                .build();

        return Flux.just(appointment1, appointment2);
    }
}
