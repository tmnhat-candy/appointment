package com.candy.appointment.resource.reactive;

import com.candy.appointment.dto.AppointmentDTO;
import com.candy.appointment.service.reactive.AppointmentRxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

@RestController
@RequestMapping("api/v1/appointment/reactive")
public class AppointmentRxResource {

    @Autowired
    AppointmentRxService appointmentService;

    @GetMapping("/query")
    ResponseEntity<Flux<AppointmentDTO>> queryAppointmentFlux() {
        return ResponseEntity.ok(appointmentService.queryRxAppointment());
    }
}
