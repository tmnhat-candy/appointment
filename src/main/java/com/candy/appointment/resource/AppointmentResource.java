package com.candy.appointment.resource;

import com.candy.appointment.dto.AppointmentDTO;
import com.candy.appointment.service.AppointmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/v1/appointment")
public class AppointmentResource {

    @Autowired
    AppointmentService appointmentService;

    @GetMapping("/query")
    ResponseEntity<List<AppointmentDTO>> queryAppointments() {
        return ResponseEntity.ok(appointmentService.queryAppointments());
    }

}
