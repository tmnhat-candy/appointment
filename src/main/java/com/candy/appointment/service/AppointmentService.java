package com.candy.appointment.service;

import com.candy.appointment.dto.AppointmentDTO;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class AppointmentService {

    public List<AppointmentDTO> queryAppointments() {
        AppointmentDTO appointment1 = AppointmentDTO.builder()
                .id(1)
                .name("appointment-reactive-1")
                .build();

        AppointmentDTO appointment2 = AppointmentDTO.builder()
                .id(2)
                .name("appointment-reactive-2")
                .build();

        return Arrays.asList(appointment1, appointment2);
    }
}
